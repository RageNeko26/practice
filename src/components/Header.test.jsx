import { describe, test, expect } from "vitest"
import { render, screen } from "@testing-library/react"
import Header from "./Header"

describe("Header test", () => {
    test("Message 'Welcome' should be shown", () => {
        // Test case
            render(<Header title="Welcome to my App"/>)
            expect(screen.getByText(/Welcome to my app/i)).toBeDefined()
    })
})